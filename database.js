const sqlite3 = require('sqlite3');

const createTask = (obj) => {
  return new Promise ((resolve, reject) => {
    const db = new sqlite3.Database('./database.sqlite');
    var valuestring = `'${obj.title}',${obj.iterations},'${obj.cronstring}','${obj.status}','${obj.url}',0`;
    var sql = `INSERT INTO tasks(title,iterations,cronstring,status,url,executed) VALUES (${valuestring})`;
    var dbid;
    db.run(sql, function(err) {
      dbid = this.lastID;
      resolve(dbid);
      if (err) {
        console.log(err.message);
        reject(err);
      }
    });
    db.close();
  });
};

const updateTask = (obj) => {
  const db = new sqlite3.Database('./database.sqlite');
  var valuestring = `title='${obj.title}',iterations=${obj.iterations},cronstring='${obj.cronstring}',status='active',url='${obj.url}'`
  var sql = `UPDATE tasks SET ${valuestring} WHERE taskid=${obj.dbID}`;
  db.run(sql, function(err) {
    if (err) {
      return console.log('Update error' + err.message);
    }
  });
  db.close();
};

const deleteTask = (taskid) => {
  const db = new sqlite3.Database('./database.sqlite');
  var sql = `DELETE FROM tasks WHERE taskid=${taskid}`;
  db.run(sql, function(err) {
    if (err) {
      return console.log('Delete error' + err.message);
    }
  });
  db.close();
}

const logExecution = (taskId, executed, iterations) => {
  const db = new sqlite3.Database('./database.sqlite');
  var closeStatement = (iterations == 0) ? ", status='complete'" : '';
  var sql = `UPDATE tasks SET executed=${executed} ${closeStatement} WHERE taskid=${taskId}`;
  db.run(sql, function(err) {
    if (err) {
      return console.log('logExecution error' + err.message);
    }
  });
  db.close();
};

const logTaskActivity = (obj) => {
    const db = new sqlite3.Database('./database.sqlite');
    var valuestring = `'${obj.title}','${obj.iterations}','${obj.cronstring}','${obj.url}','${new Date().toISOString()}'`;
    var sql = `INSERT INTO taskactivity(title,iterations,cronstring,url,dateTime) VALUES (${valuestring})`;
    db.run(sql, function(err) {
      if (err) {
        return console.log(err.message);
      }
    });
    db.close();
    logExecution(obj.dbID, obj.executed, obj.iterations);
    return this.lastID;
};

module.exports.logTaskActivity = logTaskActivity;
module.exports.createTask = createTask;
module.exports.updateTask = updateTask;
module.exports.deleteTask = deleteTask;
