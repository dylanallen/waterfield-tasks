# Recurring Task App

>A nodejs app that executes recurring tasks.

### Setup
``` bash
# If you have nvm installed, set node to version 8.11.4
nvm use

# install dependencies
npm install

# start server with nodemon hot reload at localhost:3000
npm start
```

### Schedule a Task

Submit a POST request to localhost:3000/recurringtask/ with the following data.

- title - A name for the task. Defaults to 'Anonymous Task' if not provided.
- iterations - number of times task should be executed. Defaults to 10 if not provided
- url - a url to submit as a get request. (Optional)
- second - cron syntax for seconds
- minute - cron syntax for minutes
- hour - cron syntax for hours
- dayOfMonth - cron syntax for day of month (1 - 31)
- dayOfWeek - cron syntax for day of week (1 - 7)

On execution, task info will be logged to an sqlite database, and the url post results (if provided) will be logged to the console. The task ID will be logged to the node console in cyan. You can use that id for PUT and DELETE requests described below.

### Edit a Task

Submit a PUT to **localhost:3000/recurringtask/:taskID** with the same parameters as above to overwrite a scheduled task.

### Delete a Task
Submit a DELETE to **localhost:3000/recurringtask/:taskID** to delete a task.
