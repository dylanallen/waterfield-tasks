const express = require('express');
const multer = require('multer');
const upload = multer();
const controller = require('./controller.js');
const router = express.Router();

router.get('/', (req, res) => {
  res.send("<h1>Task Manager</h1><p>Endpoint is <strong>/recurringtask/</strong></p>");
});

router.post('/recurringtask/', upload.array(), (req, res) => {
  var task = new controller.task(req, res);
  task.setupData(req, res);
  task.create(req, res).then( () => {
    res.send(task.title + ' created. Task ID: ' + task.taskID);
  });
});

router.put('/recurringtask/:taskid', upload.array(), (req, res) => {
  controller.updateTask(req, res);
  res.send('Update processing');
});

router.delete('/recurringtask/:taskid', upload.array(), (req, res) => {
  controller.deleteTask(req, res);
  res.send('Delete processing');
});

module.exports = router;
