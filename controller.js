const cron = require('node-cron');
const https = require('https');
const db = require('./database.js');

var taskList = {};

function task(req, res) {

  this.setupData = (req, res) => {
    this.title = req.body.title || 'Anonymous Task',
    this.iterations = req.body.iterations || 10,
    this.executed = this.executed || 0;
    this.second = req.body.second,
    this.minute = req.body.minute || '*',
    this.hour = req.body.hour || '*',
    this.dayOfMonth = req.body.dayOfMonth || '*',
    this.month = req.body.month || '*',
    this.dayOfWeek = req.body.dayOfWeek || '*',
    this.url = req.body.url || '',
    this.dateTime = new Date().toISOString()
    this.status = 'active';
    this.cronstring = ((this.second) ? this.second + ' ' : '') + `${this.minute} ${this.hour} ${this.dayOfMonth} ${this.month} ${this.dayOfWeek}`;
  }

  this.create = async (req, res) => {
    if (cron.validate(this.cronstring)) {
      await db.createTask(this).then((result) => {
        this.dbID = result;
        this.taskID = 'task' + this.dbID;
        taskList[this.taskID] = this;
        taskList[this.taskID].crontask = cron.schedule(this.cronstring, () => {
          runTask(this);
        });
      });
      return this.title + ' Scheduled!';
    } else {
      console.log('Invalid cron string');
      return 'Invalid Cron String';
    }
  }
}

const updateTask = (req, res) => {
  var taskobj = taskList[req.params.taskid];
  if (taskobj) {
    taskobj.setupData(req, res);
    if (cron.validate(taskobj.cronstring)) {
      taskList[req.params.taskid].crontask.destroy();
      taskList[req.params.taskid].crontask = cron.schedule(taskobj.cronstring, () => {
        runTask(taskList[req.params.taskid]);
      });
      db.updateTask(taskobj);
      console.log(req.body.title + ' updated!');
      return;
    } else {
      console.log('Invalid cron string');
      return;
    }
  } else {
    console.log('Task not found.');
    return;
  }
}

const deleteTask = (req, res) => {
  var taskobj = taskList[req.params.taskid];
  if (taskobj) {
    taskList[req.params.taskid].crontask.destroy();
    db.deleteTask(taskobj.dbID);
    delete taskList[req.params.taskid];
    console.log('Task deleted');
  }
}

const runTask = (task) => {
  console.log('\x1b[36m',task.taskID,'\x1b[0m' + ': ' + task.title + ": " + task.cronstring + " Iterations Left: " + --task.iterations);
  task.executed++;
  db.logTaskActivity(task);
  console.log('Executed at: ' + new Date().toISOString());
  if (task.url) {
    httpRequest(task.url);
  }
  if (task.iterations <= 0) {
    console.log('Destroying task: ' + task.title);
    destroyTask(task);
  }
}

const destroyTask = (task) => {
  task.status = 'complete';
  task.crontask.stop();
  task.crontask.destroy();
}

const httpRequest = (url) => {
  https.get(url, (response) => {
    let data = '';
    response.on('data', (chunk) => {
      data += chunk;
    });
    response.on('end', () => {
      console.log(JSON.parse(data));
    });
  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });
}

module.exports.task = task;
module.exports.updateTask = updateTask;
module.exports.deleteTask = deleteTask;
