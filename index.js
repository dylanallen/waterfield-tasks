const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer();
const routes = require('./routes');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use('/', routes);

app.listen(port, () => console.log(`Task app listening on port ${port}`));
